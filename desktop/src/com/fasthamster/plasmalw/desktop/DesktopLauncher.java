package com.fasthamster.plasmalw.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fasthamster.plasmalw.PlasmaLW;

public class DesktopLauncher {

	public static void main (String[] arg) {

		ColorHandler color = new ColorHandler();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 480;
		config.height = 720;
		new LwjglApplication(new PlasmaLW(color), config);
	}
}
