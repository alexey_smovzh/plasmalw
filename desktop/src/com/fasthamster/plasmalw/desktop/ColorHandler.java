package com.fasthamster.plasmalw.desktop;

import com.fasthamster.plasmalw.PlasmaColors;

/**
 * Created by alex on 04.02.17.
 */

public class ColorHandler implements PlasmaColors {

    // Return random color from pallet
    @Override
    public int getRandomColor() {
        return (0xFF << 24) | (17 << 16) | (103 << 8) | 152;
    }

    // Return random color except given
    @Override
    public int getRandomColor(int color) {
        return (0xFF << 24) | (17 << 16) | (103 << 8) | 152;
    }
}


