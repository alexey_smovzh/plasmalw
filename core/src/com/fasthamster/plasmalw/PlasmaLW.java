package com.fasthamster.plasmalw;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.Random;


public class PlasmaLW extends ApplicationAdapter {

    public static final String TAG = PlasmaLW.class.getName();
    public static final Random rand = new Random();
    public static final String PAID_PACKAGE = "com.fasthamster.plasmalw.android";
    public static final String PREFS_NAME = "settings";
    public static final String COLOR_ALIAS = "color";
    public static final int COLOR_DEFAULT = (0xFF << 24) | (17 << 16) | (103 << 8) | 152;
    public static final String SPEED_ALIAS = "speed";
    public static final float SPEED_DEFAULT = 0.5f;
    public static final String CELL_ALIAS = "cell";
    public static final int CELL_DEFAULT = 5;
    public static final String AUTO_CHANGE_COLOR_ALIAS = "autoChangeColor";
    public static final boolean AUTO_CHANGE_COLOR_DEFAULT = true;
    public static final String AUTO_CHANGE_COLOR_INTERVAL_ALIAS = "autoChangeColorIntv";
    public static final int AUTO_CHANGE_COLOR_INTERVAL_DEFAULT = 30;
    public static final int AUTO_CHANGE_COLOR_INTERVAL_MIN = 10;								// 10 seconds
    public static final String ENABLE_GYROSCOPE_ALIAS = "gyroscope";
    public static final boolean ENABLE_GYROSCOPE_DEFAULT = true;
    public static final String GYROSCOPE_THRESHOLD_ALIAS = "gyroscopeThrs";
    public static final int GYROSCOPE_THRESHOLD_DEFAULT = 5;
    public static final int GYROSCOPE_THRESHOLD_MIN = 1;
    public static final int GYROSCOPE_THRESHOLD_MAX = 10;

    private PlasmaColors colors;
    private float speed;
    private int cell;
    private float m1, m2;                                  // matrix with display ratio
    private boolean autoChangeColor;
    private float autoChangeColorIntv;

    private Touchscreen touchscreen;
    private Preferences preferences;
    private int grspThreshhold;
    private float lastColorChangeTime = 0f;
    private Vector3 color, newColor;
    private boolean gyroscopeEnabled;
    private Vector2 uvMove = new Vector2();
    private Matrix4 projection = new Matrix4();
    private Vector2 resolution = new Vector2();
    private FrameBuffer frameBuffer;
    private SpriteBatch batch;
    private ShaderProgram shader;
    private Mesh mesh;
    private float time = 0f;
    private boolean paused;
    private static boolean preferencesChanged;


    // Constructor
    public PlasmaLW(PlasmaColors colors) {
        this.colors = colors;
    }

	@Override
	public void create () {

        Gdx.app.setLogLevel(Application.LOG_NONE);

		batch = new SpriteBatch();

        // Load preferences
        preferences = Gdx.app.getPreferences(PREFS_NAME);
        setupPreferences();

        // Load shader
        shader = new ShaderProgram(Gdx.files.internal("plasma.vert").readString(),
                                   Gdx.files.internal("plasma.frag").readString());

        if (shader.getLog().length() != 0)
            Gdx.app.log(TAG, shader.getLog());

        paused = false;

	}

    @Override
	public void render() {

		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(!paused) {
// TODO: try to remove this fix
            time += Gdx.graphics.getDeltaTime();
            if(time >= 120f) time = 0f;        // Every 2 minutes renew timer

            if(autoChangeColor == true)
                updateColor();

            if(gyroscopeEnabled == true)
                updateGyroscope();

            frameBuffer.begin();
            shader.begin();
            shader.setUniformMatrix("u_projTrans", projection);
            shader.setUniformf("u_time", time);
            shader.setUniformf("u_resolution", resolution);
            shader.setUniformf("u_m1", m1);
            shader.setUniformf("u_m2", m2);
            shader.setUniformf("u_color", color);
            shader.setUniformf("u_speed", speed);
            shader.setUniformf("u_move", uvMove);
            shader.setUniformf("u_sensitivity", grspThreshhold);
            mesh.render(shader, GL20.GL_TRIANGLES);

            shader.end();
            frameBuffer.end();

            batch.setProjectionMatrix(projection);
            batch.begin();
            batch.draw(frameBuffer.getColorBufferTexture(), 0, 0, resolution.x, resolution.y);
            batch.end();

        }
	}

    @Override
    public void resize(int w, int h) {
        // check if resolution actually change
        if(w / 2 != resolution.x) {
            // Save dimensions for future use
            resolution.set(w / 2, h / 2);

            projection.setToOrtho2D(0f, 0f, resolution.x, resolution.y);

            if(mesh != null) mesh.dispose();
            mesh = createPlane(resolution);
            setCellMatrix(resolution);

            // Set new resolution to touchscreen
            if(touchscreen != null) touchscreen.setInitialPosition(getCenterOfScreen());

            // Camera and framebuffer
            if(frameBuffer != null) frameBuffer.dispose();
            frameBuffer = new FrameBuffer(Pixmap.Format.RGB888, w / 2, h / 2, false);
        }
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {

        if(preferencesChanged == true)
            setupPreferences();

        paused = false;
    }

    @Override
    public void dispose() {

        if(batch != null) batch.dispose();
        if(shader != null) shader.dispose();
        if(mesh != null) mesh.dispose();
        if(frameBuffer != null) frameBuffer.dispose();

    }

    private Vector2 getCenterOfScreen() {
        return new Vector2((float)Gdx.graphics.getWidth() / 2,
                           (float)Gdx.graphics.getHeight() / 2);
    }

    // if gyroscope enabled update handler and set light position to lightPosition variable
    private void updateGyroscope() {

        touchscreen.update();
        uvMove = touchscreen.getPosition();

    }

    // If enabled enabled allow parallax
    // set and unset touchscreenprocessor accordingly
    private void setGyroscope(boolean enabled) {

        if(enabled == false) {
            Gdx.input.setInputProcessor(null);
        } else {
            if(touchscreen == null) touchscreen = new Touchscreen();
            // this code run from setupPreferences so lightPosition now the center of the screen
            touchscreen.setInitialPosition(getCenterOfScreen());
            Gdx.input.setInputProcessor(touchscreen);
        }

        gyroscopeEnabled = enabled;

    }

    // If autocolor update enabled, changed colors
    private void updateColor() {

        lastColorChangeTime += Gdx.graphics.getDeltaTime();

        if (lastColorChangeTime >= autoChangeColorIntv) {
            int c = colors.getRandomColor();
            newColor = decodeColor(c);
            preferences.putInteger(COLOR_ALIAS, c);
            preferences.flush();

            lastColorChangeTime = 0f;
        }

        if(newColor.len() != color.len())
            color.lerp(newColor, Gdx.graphics.getDeltaTime());

    }

    private void setCellMatrix(Vector2 r) {
        if(r.x > r.y) {
            m2 = 0.1f * cell;
            m1 = m2 + 0.2f;
        } else {
            m1 = 0.1f * cell;
            m2 = m1 + 0.2f;
        }
    }

    // Setters
    public static void setPreferencesChanged(boolean state) {
        preferencesChanged = state;
    }

    private Vector3 decodeColor(int color) {
        return new Vector3((float)(color >>> 16 & 0xFF) / 255,
                           (float)(color >>> 8 & 0xFF) / 255,
                           (float)(color & 0xFF) / 255);
    }

    private void setCell(int c) {
        if(c == 0) c = 1;                       // In seekbar i can't set min value to 1
        cell = c;                               // so i need to correct it value here

        setCellMatrix(resolution);
    }

    private void setupPreferences() {

        setCell(preferences.getInteger(CELL_ALIAS, CELL_DEFAULT));

        // Color preferences
        color = decodeColor(preferences.getInteger(COLOR_ALIAS, COLOR_DEFAULT));
        newColor = color;
        speed = preferences.getFloat(SPEED_ALIAS, SPEED_DEFAULT);
        autoChangeColor = preferences.getBoolean(AUTO_CHANGE_COLOR_ALIAS, AUTO_CHANGE_COLOR_DEFAULT);
        autoChangeColorIntv = (float)preferences.getInteger(AUTO_CHANGE_COLOR_INTERVAL_ALIAS, AUTO_CHANGE_COLOR_INTERVAL_DEFAULT);

        // Gyroscope preferences
        setGyroscope(preferences.getBoolean(ENABLE_GYROSCOPE_ALIAS, ENABLE_GYROSCOPE_DEFAULT));
        // GYROSCOPE_THRESHOLD_MAX - prefValue : inverting value. lowest value slower, highest faste
        // value + GYROSCOPE_THRESHOLD_MIN : because from slider we get value in 0..9 range, we correct it to 1..10 range
        // value * 10000 : gyroscope sensibility measured in five thousands
        grspThreshhold = (GYROSCOPE_THRESHOLD_MAX - preferences.getInteger(GYROSCOPE_THRESHOLD_ALIAS, GYROSCOPE_THRESHOLD_DEFAULT) + GYROSCOPE_THRESHOLD_MIN) * 5000;

        preferencesChanged = false;
    }

    private Mesh createPlane(Vector2 r) {

        Mesh mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"));

        mesh.setVertices(new float[]{           // Vertices
                r.x, 0f, 0f,
                0f, 0f, 0f,
                0f, r.y, 0f,
                r.x, r.y, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

        return mesh;

    }
}
