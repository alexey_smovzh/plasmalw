package com.fasthamster.plasmalw;

/**
 * Created by alex on 04.02.17.
 */

public interface PlasmaColors {
    int getRandomColor();
    int getRandomColor(int color);
}
