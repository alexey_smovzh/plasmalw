package com.fasthamster.plasmalw;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by alex on 04.02.17.
 */
public class Touchscreen extends InputAdapter {

    private Vector2 position = new Vector2();
    private Vector2 centerScreen = new Vector2();

    private final static float ALPHA = 0.04f;


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        centerScreen.x = (float)screenX;
        centerScreen.y = (float)screenY;

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        position.x -= centerScreen.x - (float)screenX;
        position.y -= centerScreen.y - (float)screenY;

        return false;
    }

    // Getter
    public Vector2 getPosition() {
        return position;
    }

    // Setter
    public void setInitialPosition(Vector2 p) {

        position.set(p);

    }

    public void update() {
        // fuzzy equality testing
        if(position.epsilonEquals(centerScreen, 1f) == false)
            position.interpolate(centerScreen, ALPHA, Interpolation.linear);

    }
}
