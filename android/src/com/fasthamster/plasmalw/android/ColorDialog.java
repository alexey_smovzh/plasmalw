package com.fasthamster.plasmalw.android;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SeekBar;

import com.fasthamster.plasmalw.PlasmaLW;

/**
 * Created by alex on 10.01.17.
 */

public class ColorDialog extends Activity {

    private int color;

    private GridView colors;
    private View viewColor;
    private SeekBar redSeekBar, greenSeekBar, blueSeekBar;
    private ColorGridViewAdapter colorAdapter;

    private int red, green, blue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.color_dialog);

        // Getting information from parent activity
        Bundle extras = getIntent().getExtras();
        color = extras.getInt(SettingsActivity.COLOR_ALIAS);

        colors = (GridView) findViewById(R.id.gv_colors);
        viewColor = findViewById(R.id.v_color);
        redSeekBar = (SeekBar)findViewById(R.id.sb_red);
        greenSeekBar = (SeekBar)findViewById(R.id.sb_green);
        blueSeekBar = (SeekBar)findViewById(R.id.sb_blue);

        // Adapters
        colorAdapter = new ColorGridViewAdapter(this);
        colors.setAdapter(colorAdapter);

        // Colors gridview listener
        colors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int color = colorAdapter.getColor(i);
                viewColor.setBackgroundColor(color);
                redSeekBar.setProgress(color >>> 16 & 0xFF);
                greenSeekBar.setProgress(color >>> 8 & 0xFF);
                blueSeekBar.setProgress(color & 0xFF);
            }
        });

        // Seekbar listeners
        redSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                red = progress;
                viewColor.setBackgroundColor(Color.rgb(red, green, blue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        greenSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                green = progress;
                viewColor.setBackgroundColor(Color.rgb(red, green, blue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        blueSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                blue = progress;
                viewColor.setBackgroundColor(Color.rgb(red, green, blue));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
    }

    @Override
    public void onStart() {
        // States
        viewColor.setBackgroundColor(color);
        redSeekBar.setProgress(color >>> 16 & 0xFF);
        greenSeekBar.setProgress(color >>> 8 & 0xFF);
        blueSeekBar.setProgress(color & 0xFF);

        super.onStart();

    }

    @Override
    public void onPause() {
        LiveWallpaper.settings.setColor((0xFF << 24) | (redSeekBar.getProgress() << 16) | (greenSeekBar.getProgress() << 8) | blueSeekBar.getProgress());
        LiveWallpaper.settings.save();
        PlasmaLW.setPreferencesChanged(true);

        super.onPause();
    }
}


