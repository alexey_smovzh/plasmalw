package com.fasthamster.plasmalw.android;

import android.graphics.Color;

import com.fasthamster.plasmalw.PlasmaColors;
import com.fasthamster.plasmalw.PlasmaLW;

/**
 * Created by alex on 10.01.17.
 */

public class ColorHandler implements PlasmaColors {

    private static final int[] colors = {
            Color.rgb(58, 48, 145),
            Color.rgb(40, 55, 148),
            Color.rgb(33, 67, 156),
            Color.rgb(24, 95, 173),
            Color.rgb(18, 126, 190),
            Color.rgb(16, 160, 197),
            Color.rgb(39, 180, 102),
            Color.rgb(33, 175, 75),
            Color.rgb(74, 184, 72),
            Color.rgb(137, 197, 63),
            Color.rgb(192, 215, 47),
            Color.rgb(243, 236, 24),
            Color.rgb(251, 229, 8),
            Color.rgb(255, 200, 9),
            Color.rgb(251, 171, 24),
            Color.rgb(248, 144, 31),
            Color.rgb(245, 119, 34),
            Color.rgb(249, 98, 35),
            Color.rgb(238, 56, 35),
            Color.rgb(236, 29, 35),
            Color.rgb(228, 26, 74),
            Color.rgb(196, 26, 125),
            Color.rgb(159, 55, 150),
            Color.rgb(96, 46, 145),
    };

    private static final int[] pallet = {
            Color.rgb(101, 102, 106), Color.rgb(138, 137, 142), Color.rgb(168, 166, 170), Color.rgb(188, 187, 185),   // greyscale colors
            Color.rgb(47, 47, 137), Color.rgb(58, 48, 145), Color.rgb(75, 62, 154), Color.rgb(94, 79, 162),
            Color.rgb(40, 50, 137), Color.rgb(40, 55, 148), Color.rgb(51, 68, 156), Color.rgb(75, 88, 167),
            Color.rgb(34, 62, 146), Color.rgb(33, 67, 156), Color.rgb(44, 87, 166), Color.rgb(72, 109, 180),
            Color.rgb(9, 86, 164), Color.rgb(24, 95, 173), Color.rgb(21, 118, 187), Color.rgb(69, 140, 204),
            Color.rgb(17, 114, 169), Color.rgb(18, 126, 190), Color.rgb(29, 145, 202), Color.rgb(64, 164, 213),
            Color.rgb(22, 145, 178), Color.rgb(16, 160, 197), Color.rgb(19, 173, 207), Color.rgb(66, 190, 216),
            Color.rgb(20, 168, 92), Color.rgb(39, 180, 102), Color.rgb(60, 185, 119), Color.rgb(108, 196, 154),
            Color.rgb(24, 158, 73), Color.rgb(33, 175, 75), Color.rgb(55, 181, 74), Color.rgb(90, 187, 71),
            Color.rgb(64, 170, 72), Color.rgb(74, 184, 72), Color.rgb(106, 190, 69), Color.rgb(132, 197, 81),
            Color.rgb(126, 180, 66), Color.rgb(137, 197, 63), Color.rgb(158, 203, 60), Color.rgb(174, 209, 79),
            Color.rgb(175, 205, 55), Color.rgb(192, 215, 47), Color.rgb(201, 219, 55), Color.rgb(211, 223, 85),
            Color.rgb(230, 230, 22), Color.rgb(243, 236, 24), Color.rgb(245, 237, 48), Color.rgb(246, 238, 91),
            Color.rgb(230, 204, 29), Color.rgb(251, 229, 8), Color.rgb(250, 234, 35), Color.rgb(251, 238, 82),
            Color.rgb(228, 180, 34), Color.rgb(255, 200, 9), Color.rgb(253, 210, 46), Color.rgb(252, 220, 83),
            Color.rgb(229, 157, 36), Color.rgb(251, 171, 24), Color.rgb(253, 185, 40), Color.rgb(253, 200, 84),
            Color.rgb(230, 131, 37), Color.rgb(248, 144, 31), Color.rgb(250, 163, 47), Color.rgb(253, 180, 86),
            Color.rgb(232, 110, 37), Color.rgb(245, 119, 34), Color.rgb(247, 145, 47), Color.rgb(248, 165, 85),
            Color.rgb(230, 90, 37), Color.rgb(249, 98, 35), Color.rgb(244, 122, 47), Color.rgb(247, 148, 89),
            Color.rgb(230, 46, 36), Color.rgb(238, 56, 35), Color.rgb(240, 86, 50), Color.rgb(243, 118, 86),
            Color.rgb(233, 30, 36), Color.rgb(236, 29, 35), Color.rgb(238, 50, 49), Color.rgb(240, 86, 88),
            Color.rgb(205, 31, 68), Color.rgb(228, 26, 74), Color.rgb(234, 36, 98), Color.rgb(236, 79, 132),
            Color.rgb(180, 31, 115), Color.rgb(196, 26, 125), Color.rgb(205, 29, 140), Color.rgb(206, 77, 157),
            Color.rgb(150, 44, 145), Color.rgb(159, 55, 150), Color.rgb(163, 65, 152), Color.rgb(171, 83, 161),
            Color.rgb(89, 45, 140), Color.rgb(96, 46, 145), Color.rgb(114, 61, 151), Color.rgb(130, 78, 160)
    };

    // Return random color from pallet
    @Override
    public int getRandomColor() {
        return pallet[PlasmaLW.rand.nextInt(pallet.length)];
    }

    // Return random color except given
    @Override
    public int getRandomColor(int color) {
        int c = pallet[PlasmaLW.rand.nextInt(pallet.length)];
        while (color == c) {
            c = pallet[PlasmaLW.rand.nextInt(pallet.length)];
        }
        return c;
    }

    // Return color from colors
    public static int getColor(int idx) {
        return colors[idx];
    }

    public static int getColorCount() {
        return colors.length;
    }

}

