package com.fasthamster.plasmalw.android;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasthamster.plasmalw.PlasmaLW;

/**
 * Created by alex on 14.03.16.
 */
public class SettingsHandler {

    Context context;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    private int color;
    private float speed;
    private int cell;
    private boolean autoColor;
    private int autoColorInterval;
    private boolean gyroscopeEnable;
    private int gyroscopeThreshold;

    private boolean colorChanged;
    private boolean speedChanged;
    private boolean cellChanged;
    private boolean autoColorChanged;
    private boolean autoColorIntvChanged;
    private boolean gyroscopeEnableChanged;
    private boolean gyroscopeThresholdChanged;


    public SettingsHandler(Context context) {
        this.context = context;
        load();
    }

    public void load() {

        settings = context.getSharedPreferences(PlasmaLW.PREFS_NAME, 0);
        color = settings.getInt(PlasmaLW.COLOR_ALIAS, PlasmaLW.COLOR_DEFAULT);
        speed = settings.getFloat(PlasmaLW.SPEED_ALIAS, PlasmaLW.SPEED_DEFAULT);
        cell = settings.getInt(PlasmaLW.CELL_ALIAS, PlasmaLW.CELL_DEFAULT);
        autoColor = settings.getBoolean(PlasmaLW.AUTO_CHANGE_COLOR_ALIAS, PlasmaLW.AUTO_CHANGE_COLOR_DEFAULT);
        autoColorInterval = settings.getInt(PlasmaLW.AUTO_CHANGE_COLOR_INTERVAL_ALIAS, PlasmaLW.AUTO_CHANGE_COLOR_INTERVAL_DEFAULT);
        gyroscopeEnable = settings.getBoolean(PlasmaLW.ENABLE_GYROSCOPE_ALIAS, PlasmaLW.ENABLE_GYROSCOPE_DEFAULT);
        gyroscopeThreshold = settings.getInt(PlasmaLW.GYROSCOPE_THRESHOLD_ALIAS, PlasmaLW.GYROSCOPE_THRESHOLD_DEFAULT);

        clearStates();

    }

    public void save() {

        editor = settings.edit();
        editor.putInt(PlasmaLW.COLOR_ALIAS, color);
        editor.putFloat(PlasmaLW.SPEED_ALIAS, speed);
        editor.putInt(PlasmaLW.CELL_ALIAS, cell);
        editor.putBoolean(PlasmaLW.AUTO_CHANGE_COLOR_ALIAS, autoColor);
        editor.putInt(PlasmaLW.AUTO_CHANGE_COLOR_INTERVAL_ALIAS, autoColorInterval);
        editor.putBoolean(PlasmaLW.ENABLE_GYROSCOPE_ALIAS, gyroscopeEnable);
        editor.putInt(PlasmaLW.GYROSCOPE_THRESHOLD_ALIAS, gyroscopeThreshold);
        editor.commit();

        clearStates();

    }

    private void clearStates() {
        colorChanged = false;
        speedChanged = false;
        cellChanged = false;
        autoColorChanged = false;
        autoColorIntvChanged = false;
        gyroscopeEnableChanged = false;
        gyroscopeThresholdChanged = false;
    }

    public boolean isPreferencesChanged() {

        if(colorChanged == true) return true;
        if(speedChanged == true) return true;
        if(cellChanged == true) return true;
        if(autoColorChanged == true) return true;
        if(autoColorIntvChanged == true) return true;
        if(gyroscopeEnableChanged == true) return true;
        return gyroscopeThresholdChanged == true;

    }


    public int getColor() {
        return color;
    }
    public float getSpeed() {
        return speed;
    }
    public int getCell() {
        return  cell;
    }
    public boolean getAutoColor() {
        return autoColor;
    }
    public int getAutoColorInterval() {
        return autoColorInterval;
    }
    public boolean getGyroscopeEnable() {
        return gyroscopeEnable;
    }
    public int getGyroscopeThreshold() {
        return gyroscopeThreshold;
    }

    public void setColor(int c) {
        this.color = c;
        this.colorChanged = true;
    }
    public void setSpeed(float s) {
        this.speed = s;
        this.speedChanged = true;
    }
    public void setCell(int l) {
        this.cell = l;
        this.cellChanged = true;
    }
    public void setAutoColor(boolean a) {
        this.autoColor = a;
        this.autoColorChanged = true;
    }
    public void setAutoColorInterval(int i) {
        this.autoColorInterval = i;
        this.autoColorIntvChanged = true;
    }
    public void setGyroscopeEnable(boolean g) {
        this.gyroscopeEnable = g;
        this.gyroscopeEnableChanged = true;
    }
    public void setGyroscopeThreshold(int t) {
        this.gyroscopeThreshold = t;
        this.gyroscopeThresholdChanged = true;
    }
}
