package com.fasthamster.plasmalw.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.fasthamster.plasmalw.PlasmaLW;

/**
 * Created by alex on 14.03.16.
 */
public class SettingsActivity extends Activity {

    private LinearLayout color;
    private View colorResult;
    private SeekBar speedBar;
    private TextView speedResult;
    private SeekBar cellBar;
    private TextView cellResult;
    private CheckBox autoColor;
    private SeekBar autoColorInterval;
    private TextView autoColorIntervalTitle, autoColorIntervalResult;
    private RelativeLayout autoColorIntervalLegend;
    private CheckBox enableGrsp;
    private SeekBar grspThreshold;
    private TextView grspThresholdTitle, grspThresholdResult;
    private RelativeLayout grspThresholdLegend;
    private LinearLayout rateMe;


    public static final String COLOR_ALIAS = "com.fasthamster.plasmalw.COLOR";


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Layouts
        setContentView(R.layout.settings);
        speedBar = (SeekBar)findViewById(R.id.speed_bar);
        speedResult = (TextView)findViewById(R.id.tv_result);
        cellBar = (SeekBar)findViewById(R.id.cell_bar);
        cellResult = (TextView)findViewById(R.id.tv_cell_result);
        color = (LinearLayout)findViewById(R.id.ll_color);
        colorResult = findViewById(R.id.v_color);
        autoColor = (CheckBox)findViewById(R.id.chk_autocolor_change);
        autoColorInterval = (SeekBar)findViewById(R.id.sb_autocolor_interval);
        autoColorIntervalTitle = (TextView)findViewById(R.id.tv_autocolor_interval_title);
        autoColorIntervalResult = (TextView)findViewById(R.id.tv_color_autochange_interval_result);
        autoColorIntervalLegend = (RelativeLayout)findViewById(R.id.rl_autocolor_interval_legend);
        enableGrsp = (CheckBox)findViewById(R.id.chk_gyroscope_enable);
        grspThreshold = (SeekBar)findViewById(R.id.sb_gyroscope_threshold);
        grspThresholdTitle = (TextView)findViewById(R.id.tv_gyroscope_threshold_title);
        grspThresholdResult = (TextView)findViewById(R.id.tv_gyroscope_threshold_result);
        grspThresholdLegend = (RelativeLayout)findViewById(R.id.rl_gyroscope_threshold_legend);
        rateMe = (LinearLayout) findViewById(R.id.btn_rate);


        // Color choose listeners
        color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ColorDialog.class);
                intent.putExtra(COLOR_ALIAS, LiveWallpaper.settings.getColor());
                startActivityForResult(intent, 1);
            }
        });

        // Speed SeekBar listener
        speedBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                LiveWallpaper.settings.setSpeed(speed2float(progress));
                speedResult.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Cell size SeekBar listener
        cellBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                LiveWallpaper.settings.setCell(progress);
                cellResult.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Autochange color checkbox listener
        autoColor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setAutoColor(b);
                automaticColorIntervalVisible(b);
            }
        });

        // Autochange color seekbar listener
        autoColorInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if(progress < PlasmaLW.AUTO_CHANGE_COLOR_INTERVAL_MIN)
                    progress = PlasmaLW.AUTO_CHANGE_COLOR_INTERVAL_MIN;
                // only on user input
                if(b == true) {
                    LiveWallpaper.settings.setAutoColorInterval(progress);
                    autoColorIntervalResult.setText(convertToMinutes(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        // gyroscope enable checkbox listener
        enableGrsp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LiveWallpaper.settings.setGyroscopeEnable(b);
                gyroscopeSettingsVisible(b);
            }
        });

        // gyroscope threshold listener
        grspThreshold.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                if(b == true) {
                    LiveWallpaper.settings.setGyroscopeThreshold(progress);
                    grspThresholdResult.setText(String.valueOf(progress + PlasmaLW.GYROSCOPE_THRESHOLD_MIN));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        // rateme button listener
        rateMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent market = new Intent(Intent.ACTION_VIEW);
                market.setData(Uri.parse("market://details?id=" + PlasmaLW.PAID_PACKAGE));

                Intent website = new Intent(Intent.ACTION_VIEW);
                website.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + PlasmaLW.PAID_PACKAGE));

                try {
                    startActivity(market);
                } catch (ActivityNotFoundException e) {
                    startActivity(website);
                }
            }
        });

    }

    private int speed2int(float s) {
        return (int)(s * 100);
    }

    private float speed2float(int s) {
        return (float)s / 100f;
    }

    @Override
    public void onStart() {

        super.onStart();
        // load states
        speedBar.setProgress(speed2int(LiveWallpaper.settings.getSpeed()));
        speedResult.setText(Integer.toString(speed2int(LiveWallpaper.settings.getSpeed())));
        cellBar.setProgress(LiveWallpaper.settings.getCell());
        cellResult.setText(Integer.toString(LiveWallpaper.settings.getCell()));
        colorResult.setBackgroundColor(LiveWallpaper.settings.getColor());
        autoColor.setChecked(LiveWallpaper.settings.getAutoColor());
        automaticColorIntervalVisible(LiveWallpaper.settings.getAutoColor());
        autoColorInterval.setProgress(LiveWallpaper.settings.getAutoColorInterval());
        autoColorIntervalResult.setText(convertToMinutes(LiveWallpaper.settings.getAutoColorInterval()));

        enableGrsp.setChecked(LiveWallpaper.settings.getGyroscopeEnable());
        grspThreshold.setProgress(LiveWallpaper.settings.getGyroscopeThreshold());
        grspThresholdResult.setText(String.valueOf(LiveWallpaper.settings.getGyroscopeThreshold() + PlasmaLW.GYROSCOPE_THRESHOLD_MIN));
        gyroscopeSettingsVisible(LiveWallpaper.settings.getGyroscopeEnable());

    }


    @Override
    public void onPause() {
        // Check if preferences was actually changed
        if(LiveWallpaper.settings.isPreferencesChanged() == true) {
            LiveWallpaper.settings.save();
            PlasmaLW.setPreferencesChanged(true);
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1)
            colorResult.setBackgroundColor(LiveWallpaper.settings.getColor());

    }

    private void gyroscopeSettingsVisible(boolean state) {

        grspThreshold.setEnabled(state);
        grspThresholdTitle.setEnabled(state);
        grspThresholdResult.setEnabled(state);
        disableEnableControls(grspThresholdLegend, state);

    }


    private void automaticColorIntervalVisible(boolean state) {
        autoColorInterval.setEnabled(state);
        autoColorIntervalTitle.setEnabled(state);
        autoColorIntervalResult.setEnabled(state);
        disableEnableControls(autoColorIntervalLegend, state);
    }

    private String convertToMinutes(int seconds) {

        if(seconds < 60) {
            return String.format("0:%02d", seconds);
        } else {
            return String.format("%01d:%02d", seconds / 60, seconds % 60);
        }
    }

    // from http://stackoverflow.com/questions/7068873/how-can-i-disable-all-views-inside-the-layout
    private void disableEnableControls(ViewGroup vg, boolean enable){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls((ViewGroup)child, enable);
            }
        }
    }
}
