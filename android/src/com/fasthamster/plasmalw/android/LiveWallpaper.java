package com.fasthamster.plasmalw.android;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;
import com.fasthamster.plasmalw.PlasmaColors;
import com.fasthamster.plasmalw.PlasmaLW;

/**
 * Created by alex on 14.03.16.
 */

public class LiveWallpaper extends AndroidLiveWallpaperService {

    public static SettingsHandler settings;
    private ColorHandler color;
    private PlasmaLWListener listener;


    @Override
    public void onCreateApplication() {

        super.onCreateApplication();

        // Colors
        color = new ColorHandler();

        // Settings staff
        settings = new SettingsHandler(getApplicationContext());

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useWakelock = false;
        config.getTouchEventsForLiveWallpaper = false;
        config.useAccelerometer = false;

        listener = new PlasmaLWListener(color);

        initialize(listener, config);

    }

    @Override
    public Engine onCreateEngine() {
        return new AndroidWallpaperEngine(){
            @Override
            public void onPause(){
                super.onPause();
                listener.pause();
            }

            @Override
            public void onResume(){
                super.onResume();
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public static class PlasmaLWListener extends PlasmaLW implements AndroidWallpaperListener {

        public PlasmaLWListener(PlasmaColors colors) {
            super(colors);
        }

        @Override
        public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

        }

        @Override
        public void previewStateChange(boolean isPreview) {

        }
    }
}
