package com.fasthamster.plasmalw.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * Created by alex on 10.01.17.
 */

public class ColorGridViewAdapter extends BaseAdapter {

    private Context context;

    // Constructor
    public ColorGridViewAdapter(Context context) {

        this.context = context;

    }

    // Getters
    public int getColor(int i) {
        return ColorHandler.getColor(i);
    }

    @Override
    public int getCount() {
        return ColorHandler.getColorCount();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ImageView imageView;
        View v = view;

        if (v == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            v = inflater.inflate(R.layout.grid_cell, viewGroup, false);
            imageView = (ImageView)v.findViewById(R.id.iv_cell);
            v.setTag(imageView);
        } else {
            imageView = (ImageView)v.getTag();
        }

        imageView.setBackgroundColor(ColorHandler.getColor(i));
        imageView.setId(i);

        return v;

    }
}
