#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision lowp float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif

#define b mat2(0.3, -0.3, 0.3, 0.3)

uniform vec2 u_resolution;
uniform vec3 u_color;
uniform vec2 u_move;
uniform float u_sensitivity;
uniform float u_m1;
uniform float u_m2;
uniform float u_speed;

varying float v_time;


float worley(float t, vec2 q, mat2 m) {

    q.x+=5.0;
    vec2 p = 10.*q*m;

    vec2 pi = floor(p);
    vec4 v = vec4(pi.xy, pi.xy + 1.0);
    v -= 64.*floor(v*0.015);
    v.xz = v.xz*1.435 + 34.423;
    v.yw = v.yw*2.349 + 183.37;
    v = v.xzxz*v.yyww;
    v *= v;

    v *= t*0.000004 + 0.5;
    vec4 vx = 0.25*sin(fract(v*0.00047)*6.2831853);
    vec4 vy = 0.25*sin(fract(v*0.00074)*6.2831853);

    vec2 pf = p - pi;
    vx += vec4(0., 1., 0., 1.) - pf.xxxx;
    vy += vec4(0., 0., 1., 1.) - pf.yyyy;
    v = vx*vx + vy*vy;
    v.xy = min(v.xy, v.zw);

    return min(v.x, v.y);
}

void main(void) {

    float t = v_time * u_speed;
    vec3 c = u_color * 0.3;
    vec3 f = u_color * 5.0;

    mat2 m = mat2(u_m1, -u_m2, u_m1, u_m2);

	vec2 d = (gl_FragCoord.xy + t*32.0)/u_resolution.xy;
	vec2 u = (gl_FragCoord.xy + t*-32.0)/u_resolution.xy;

    vec2 s = u_move / u_sensitivity;
    d -= s;
    u -= s;

    vec3 r = mix(c, u_color, worley(t, d, m));
    vec3 l = mix(c, f, worley(t, u, b));

    vec2 p = (gl_FragCoord.xy / u_resolution.xy) - vec2(0.5);
    float v = smoothstep(0.75, 0.2, length(p));

	gl_FragColor = vec4(sqrt(r*l)*v, 1.0);

}


